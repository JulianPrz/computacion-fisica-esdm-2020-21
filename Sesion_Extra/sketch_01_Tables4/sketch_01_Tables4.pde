ArrayList <Ball> balls;

Table table;

void setup() {
  size(640, 360, FX2D);
  background(255);
  balls = new ArrayList();
  loadData();
}

void draw() {
  background(255);

  for (int i = 0; i < balls.size(); i++) { // iteramos en función del tamaño de nuestra lista para mostrar todas las bolas
    Ball b = balls.get(i); //Llama a cada bola de nuestra lista
    b.display(); 
    b.move();
    b.checkEdges();
  }
}

void loadData() {
  table = loadTable("data.csv", "header");

  for (TableRow row : table.rows()) { //table.rows() devuelve el índice de cada fila, ya que utiliza el for
    float x = row.getFloat("x");         //Columna 0
    float y = row.getFloat("y");         //Columna 1
    float d = row.getFloat("diametro");  //Columna 2
    String cs = row.getString("color");  //Columna 3

    color c = color(red(unhex(cs)), green(unhex(cs)), blue(unhex(cs)));
    println(x, y, d, cs);
    //noStroke();
    //fill(c);
    //ellipse(x, y, d, d);
    Ball b = new Ball(x, y, d, c);
    balls.add(b);
  }
}

void mousePressed() {
  TableRow row = table.addRow();
  float x = mouseX;
  float y = mouseY;
  float d = random(40, 80);
  row.setFloat("x", x);
  row.setFloat("y", y);
  row.setFloat("diametro", d);
  color c = color(random(255), random(255), random(255));
  String cs = hex(c);
  row.setString("color", cs);

  Ball b = new Ball(x, y, d, c);
  balls.add(b);

  if (table.getRowCount() > 10) {
    // Delete the oldest row
    table.removeRow(0);
    balls.remove(0);
  }
  saveTable(table, "data/data.csv");

  //loadData();
}
