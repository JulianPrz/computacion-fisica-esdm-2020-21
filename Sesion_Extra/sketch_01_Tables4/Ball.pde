class Ball {
  // variables propias del objeto
  float x, y, w;
  color c;

  float vx, vy; //variables para velocidad

  //Constructor
  Ball(float _x, float _y, float _w, color _c) {
    x = _x;
    y = _y;
    w = _w;
    c = _c;

    vx = random(-1,1);
    vy = random(-1,1);
  }

  void display() {
    fill(c);
    noStroke();
    ellipse(x, y, w, w);
  }

  void move() {
    x += vx;
    y += vy;
  }

  void checkEdges() {
    if (x > width) {
      x = 0;
    }

    if (x < 0) {
      x = width;
    }

    if (y > height) {
      y = 0;
    }

    if (y < 0) {
      y = height;
    }
  }
}
