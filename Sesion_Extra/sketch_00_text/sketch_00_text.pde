
int[] values;

void setup() {
  size(400, 200);
  String[] lines = loadStrings("data.txt");
  values = int(split(lines[0], ','));
  printArray(values);

  for (int i = 0; i<values.length; i++) {
    fill(values[i]*2);
    ellipse(50+i*75, height/2, values[i], values[i]);
  }
}
