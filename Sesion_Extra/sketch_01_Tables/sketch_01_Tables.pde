Table table;

void setup() {
  size(640, 360);
  background(255);
  table = loadTable("data.csv", "header");
  
  for (TableRow row : table.rows()) {
    float x = row.getFloat("x");         //0
    float y = row.getFloat("y");         //1
    float d = row.getFloat("diametro");  //2
    String cs = row.getString("color");  //3
    
    color c = color(red(unhex(cs)),green(unhex(cs)),blue(unhex(cs)));
    println(x,y,d,cs);
    noStroke();
    fill(c);
    ellipse(x,y,d,d);
  }
}
