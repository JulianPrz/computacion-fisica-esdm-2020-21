Table table;

table = loadTable("https://raw.githubusercontent.com/numeroteca/tarjetasblack/master/data/data.tsv", "header");

for (TableRow row : table.rows()) {
  String quien = row.getString("quien");
  String date = row.getString("date");
  String hora = row.getString("hora");
  int importe = row.getInt("importe");
  String actividad = row.getString("actividad");

  println(quien+" el día "+date+" a las "+hora+" se ha gastado "+importe+"€ en "+actividad);
}
