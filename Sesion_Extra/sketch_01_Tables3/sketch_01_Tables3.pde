Ball[] balls;

Table table;

void setup() {
  size(640, 360,FX2D);
  background(255);
  loadData();
}

void draw() {
  background(255);
  
  for (Ball b : balls) {
    b.display();
    b.move();
    b.checkEdges();
  }
}

void loadData() {
  table = loadTable("data.csv", "header");

  balls = new Ball[table.getRowCount()]; //Crea tantas bolas en la lista como filas tenga la tabla

  int rowCount = 0;
  for (TableRow row : table.rows()) { //table.rows() devuelve el índice de cada fila, ya que utiliza el for
    float x = row.getFloat("x");         //Columna 0
    float y = row.getFloat("y");         //Columna 1
    float d = row.getFloat("diametro");  //Columna 2
    String cs = row.getString("color");  //Columna 3

    color c = color(red(unhex(cs)), green(unhex(cs)), blue(unhex(cs)));
    println(x, y, d, cs);
    //noStroke();
    //fill(c);
    //ellipse(x, y, d, d);

    balls[rowCount] = new Ball(x, y, d, c);
    rowCount++;
  }
}

void mousePressed() {

  TableRow row = table.addRow();
  row.setFloat("x", mouseX);
  row.setFloat("y", mouseY);
  row.setFloat("diametro", random(40, 80));
  color c = color(random(255), random(255), random(255));
  String cs = hex(c);
  row.setString("color", cs);

  saveTable(table, "data/data.csv");

  //loadData();
}
