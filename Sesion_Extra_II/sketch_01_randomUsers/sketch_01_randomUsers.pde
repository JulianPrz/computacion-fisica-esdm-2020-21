PShape flag;
PImage faceImg;
String firstName,lastName;

JSONObject jsonName;
JSONArray jsonFlag;
JSONObject jsonFace;

String apiKey = "Aquí tu clave API"; //Tienes que registrarte en https://generated.photos/ para obtener una

void setup() {
  size(360, 360);
  textAlign(CENTER);
  loadData();
} 
void mousePressed(){
  loadData();
}
void draw() {
  background(0);
  translate(width/2,height/2);
  image(faceImg, -126/2, -126, 126, 126);
  shape(flag, 126/2, -126, 45, 31.5);
  textSize(36);
  text(firstName,0,40);
  text(lastName,0,80);
}

void loadData() {
  //NAME
  jsonName = loadJSONObject("https://randomuser.me/api/?inc=gender,name,location");
  JSONArray results = jsonName.getJSONArray("results");
  JSONObject user = results.getJSONObject(0);
  String gender = user.getString("gender");
  JSONObject name = user.getJSONObject("name");
  firstName = name.getString("first");
  lastName = name.getString("last");
  JSONObject location = user.getJSONObject("location");
  String country = location.getString("country");
  println(firstName, lastName, gender, country);
  //FLAG
  jsonFlag = loadJSONArray("https://restcountries.eu/rest/v2/name/"+country);
  JSONObject result = jsonFlag.getJSONObject(0);
  String flagURL = result.getString("flag");
  flag = loadShape(flagURL);
  // FACE
  jsonFace = loadJSONObject("https://api.generated.photos/api/v1/faces?gender="+gender+"&api_key="+apiKey);
  JSONArray faces = jsonFace.getJSONArray("faces");
  JSONObject face = faces.getJSONObject((int)random(10)); //Siempre me devuelve 10 caras, es el mínimo, pido una random
  JSONArray urls = face.getJSONArray("urls");
  JSONObject resolutions = urls.getJSONObject(2);
  String stringURL = resolutions.getString("128");
  println(stringURL);
  faceImg = loadImage(stringURL, "jpg");
}
