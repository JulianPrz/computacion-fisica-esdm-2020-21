PImage mask;
PImage face;

void setup() {
  size(400, 267);
  mask = loadImage("mask.png");
  face = loadImage("https://dl.dropboxusercontent.com/s/07cxlk0u7t1zm1m/before.jpg");
  face.mask(mask);
}

void draw() {
  background(0);
  image(face, mouseX-200, mouseY-133);
}
