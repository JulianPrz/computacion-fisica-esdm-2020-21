JSONObject json;
float t, h;
String city;
String apiKey="Aquí tu clave API"; //Tienes que registrarte en https://openweathermap.org/ para obtener una

void setup() {
  size(200, 100);
  loadData();
}

void loadData() {
  int r = (int)random(4);
  String[] randomCity = {"Madrid", "Barcelona", "Paris", "London", "Newyork"};
  city = randomCity[r];
  json = loadJSONObject("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+apiKey+"&units=metric");
  JSONObject main = json.getJSONObject("main"); 
  t = main.getFloat("temp");
  h = main.getInt("humidity");
  println("Temperatura de "+t+"ºC"+" y una humedad del "+h+"%"+" en "+city);
}

void draw() {
  if (frameCount%(60*5)==0) {
    loadData();
  }

  background(255);
  noStroke();
  fill(#F53E5F);
  rect(0, height/2-5, t*5, 10);
  text(t+"ºC", t*5+10, height/2+3);

  fill(#167DF7);
  rect(0, height/2+5, h*2, 10);
  text(h+"%", h*2+5, height/2+15);
}
