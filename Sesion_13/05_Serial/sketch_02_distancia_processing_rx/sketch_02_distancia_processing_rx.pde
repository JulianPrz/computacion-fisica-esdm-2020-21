import processing.serial.*;
Serial myPort;
float val;

void setup() {
  size(300, 300);
  println(Serial.list());
  myPort = new Serial(this, "AQUI NOMBRE DE TU PUERTO", 9600);
  noStroke();
  background(255, 0, 0);
}

void draw() {
  //ESTADO
  while (myPort.available() > 0) {
    String inBuffer = myPort.readStringUntil('\n');   
    if (inBuffer != null) {
      //println(inBuffer);
      val = constrain(float(inBuffer), 4, 400);
      //println(val);
      if ( val >=4 && val <= 20){
        fill(255);
      }else{
        fill(0);
      }
      rect(50, 50, 200, 200);
    }
  }

  //RANGO
  while (myPort.available() > 0) {
    String inBuffer = myPort.readStringUntil('\n');   
    if (inBuffer != null) {
      //println(inBuffer);
      val = constrain(float(inBuffer), 4, 400);
      //println(val);
      val = map(val, 4, 20, 0, 255);
      fill(val);
      rect(50, 50, 200, 200);
    }
  }
}
