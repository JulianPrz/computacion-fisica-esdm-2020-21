int led = 13;
int button1 = 12;
int button2 = 11;
void setup() {
pinMode(led, OUTPUT);
pinMode(button1, INPUT_PULLUP);
pinMode(button2, INPUT_PULLUP);
Serial.begin(9600);
}
void loop(){
  //ARDUINO RX
if(Serial.available() > 0) {
char ledState = Serial.read();
if(ledState == '1'){
digitalWrite(led, HIGH);
}
if(ledState == '0'){
digitalWrite(led, LOW);
}
}

//ARDUINO TX
int buttonState1 = digitalRead(button1);
if ( buttonState1 == LOW){
Serial.write(1);
delay(500);
}

int buttonState2 = digitalRead(button2);
if ( buttonState2 == LOW){
Serial.write(2);
delay(500);
}

}
