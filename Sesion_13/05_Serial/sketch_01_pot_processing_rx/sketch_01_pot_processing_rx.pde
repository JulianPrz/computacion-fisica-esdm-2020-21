import processing.serial.*;
Serial myPort;
float val;

void setup() {
  size(300, 300);
  println(Serial.list());
  myPort = new Serial(this, "AQUÍ TU NOMBRE DEL PUERTO", 9600); // Cámbia el nombre de tu puerto
  noStroke();
  background(255, 0, 0);
}

void draw() {
  while (myPort.available() > 0) {
    String inBuffer = myPort.readStringUntil('\n');  //Lee el string que recibe por serial hasta el salto de linea (\n)
    if (inBuffer != null) { //Si lo que llega por serial es un dato válido...
      //println(inBuffer);
      val = float(inBuffer)/4;
      fill(val);
      rect(50, 50, 200, 200);
    }
  }
}
