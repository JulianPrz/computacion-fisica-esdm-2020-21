import processing.serial.*;
Serial myPort;
boolean rect1 = false;
boolean rect2 = false;
int val;

void setup() {
  size(300, 300);
  myPort = new Serial(this, "/dev/ttyACM0", 9600);
  noStroke();
}

void draw() {
  background(255); 
  //PROCESSING RX
  if ( myPort.available() > 0) { 
    val = myPort.read();
    println(val);
  }

  if (val == 1) {            
    rect1 = !rect1;
    val = 0;
  } 
  if (val == 2) {                      
    rect2 = !rect2;
    val=0;
  }

  if (rect1) {
    fill(0);
    rect(0, 0, width/2, height);
  } else {
    fill(255);
    rect(0, 0, width/2, height);
  }

  if (rect2) {
    fill(0);
    rect(width/2, 0, width, height);
  } else {
    fill(255);
    rect(width/2, 0, width, height);
  }
  
  //PROCESSING TX
  if (mousePressed && (mouseButton == LEFT)) {
    myPort.write('1');
  }
  if (mousePressed && (mouseButton == RIGHT)) {
    myPort.write('0');
  }
}
