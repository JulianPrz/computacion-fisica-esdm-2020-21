unsigned long tiempo=0;
unsigned long tiempoLed=0;
int blink=500;
boolean estado=false;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  tiempo = millis();
  if(tiempo >= (tiempoLed+blink)){
    tiempoLed=tiempo;
    estado = !estado;
    digitalWrite(LED_BUILTIN,estado);
    }
}
