int estado  = 0; // HIGH = 1  LOW = 0

int led     = 5;    //Pin digital 6 para el led del Sensor Distancia (PWM)
int Trigger = 13;    //Pin digital 9 para el Trigger del sensor
int Echo    = 12;   //Pin digital 10 para el Echo del sensor

int i;

void setup() {
  Serial.begin(9600);

  pinMode(led, OUTPUT);

  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);

  digitalWrite(Trigger, LOW);
}

void loop() {
  //////////SENSORDISTANCIA////////////////////////////////
  long t; //tiempo que demora en llegar el eco
  long d; //distancia en centimetros

  digitalWrite(Trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          //Enviamos un pulso de 10us

  t = pulseIn(Echo, HIGH); //obtenemos el tiempo que tarda en llegar el pulso HIGH en microsegundos
  d = t * 0.034 / 2;       //calculamos la distancia del objeto en base a la constante de la velocidad del sonido en el aire

  //brillo = map(d,2,400,0,255); //mapeo distancia brillo
  //analogWrite(led,brillo);

  if (d >= 20 && d <= 150) { //Entrada en un rango de distancia
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }

  Serial.print("Distancia: ");
  Serial.print(d);         //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(50);              //Hacemos una pausa de 100ms
}
