int Trigger = 9;  //Pin digital 9 para el Trigger del sensor
int Echo = 10;    //Pin digital 10 para el Echo del sensor
int Buzzer = 3;   //Pin digital 3 para el Buzzer

long t; //tiempo que demora en llegar el eco
long d; //distancia en centimetros

void setup() {
  Serial.begin(9600);//iniciailzamos la comunicación serial
  pinMode(Trigger, OUTPUT); //pin Trigger como salida
  pinMode(Echo, INPUT);  //pin Echo como entrada
  pinMode(Buzzer,OUTPUT);
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void loop(){
  digitalWrite(Trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          //Enviamos un pulso de 10us
  
  t = pulseIn(Echo, HIGH); //obtenemos el tiempo que tarda en llegar el pulso HIGH en microsegundos
  d = t*0.034/2;             //calculamos la distancia del objeto en base a la constante de la velocidad del sonido en el aire

  Serial.print("Distancia: ");
  Serial.print(d);      //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(100);          //Hacemos una pausa de 100ms entre datos

  if (d >= 0d && <= 20){ // si distancia entre 0 y 20 cms.
  digitalWrite(Buzzer, HIGH);      // suena el zumbador
  delay(d * 10);      // demora proporcional a la distancia
  digitalWrite(Buzzer, LOW);     // apaga el zumbador
  }
}
