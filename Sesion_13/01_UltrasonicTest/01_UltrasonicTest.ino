int Trigger = 3;             //Pin digital 2 para el Trigger del sensor
int Echo = 2;               //Pin digital 3 para el Echo del sensor

void setup() {
  Serial.begin(9600);        //iniciailzamos la comunicación serial
  pinMode(Trigger, OUTPUT);  //pin Trigger como salida
  pinMode(Echo, INPUT);      //pin Echo como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void loop()
{

  long t;                    //tiempo que demora en llegar el eco
  long d;                    //distancia en centimetros

  digitalWrite(Trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);     //Enviamos un pulso de 10microsegundos


  t = pulseIn(Echo, HIGH);   //obtenemos el tiempo que tarda en llegar el pulso HIGH en microsegundos
  d = t * 0.034 / 2;         //calculamos la distancia del objeto en base a la constante de la velocidad del sonido en el aire

  Serial.print("Distancia: ");
  Serial.print(d);           //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(50);                //Hacemos una pausa de 100ms
}
