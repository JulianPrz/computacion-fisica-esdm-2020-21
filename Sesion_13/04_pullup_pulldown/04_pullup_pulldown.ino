int button = 11;
int led = 13;

void setup() {
  pinMode(button, INPUT);
  //pinMode(button, INPUT_PULLUP);
  //pinMode(button, INPUT_PULLDOWN);
  pinMode(led, OUTPUT);
}

void loop() {
  if (digitalRead(button) == HIGH) {
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
}
