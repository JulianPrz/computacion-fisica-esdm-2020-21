int Trigger = 3;             //Pin digital 2 para el Trigger del sensor
int Echo = 2;               //Pin digital 3 para el Echo del sensor

void setup() {
  Serial.begin(9600);        //iniciailzamos la comunicación serial
  pinMode(Trigger, OUTPUT);  //pin Trigger como salida
  pinMode(Echo, INPUT);      //pin Echo como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void loop()
{

  long t;                    //tiempo que demora en llegar el eco
  long d;                    //distancia en centimetros

  digitalWrite(Trigger, LOW);
  delay(1);
  digitalWrite(Trigger, HIGH);

  t = pulseIn(Echo, HIGH);   //obtenemos el tiempo que tarda en llegar el pulso HIGH en microsegundos
  d = t / 58.2;         //calculamos la distancia del objeto en base a la constante de la velocidad del sonido en el aire

  Serial.print("Distancia: ");
  Serial.print(d);           //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(100);                //Hacemos una pausa de 100ms
}
