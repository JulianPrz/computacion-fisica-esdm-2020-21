int gridX = 200;                       // número de puntos horizontales en el grid
int gridY = 75;                        // número de puntos verticales en el grid

color BACKGROUND_COLOR = color(255);   // color de fondo del sketch
color PGRAPHICS_COLOR = color(0);      // color de la tipografía en PGraphics

PFont f;

PGraphics pg;

void setup() {
  size(1280, 720, FX2D);
  f = createFont("Liberation Sans Bold", 500);
  //print(PFont.list());

  pg = createGraphics(width, height); 
  pg.beginDraw();
  pg.textAlign(CENTER, CENTER);
  pg.fill(PGRAPHICS_COLOR);
  pg.textFont(f);
  pg.text("OLA", pg.width/2, pg.height/2); 
  pg.endDraw();
}

void draw() {
  background(BACKGROUND_COLOR);
  //image(pg, 0, 0);

  float w = float(width) / gridX; 
  float h = float(height) / gridY;
   translate(w/2, h/2);

  boolean continuous = false; //Flag para preguntar continuidad del siguiente punto cuando está inText

  for (int y=0; y<gridY; y++) {   // Por cada fila
    for (int x=0; x<gridX; x++) {  // Va pasando por todas las columnas

      float vx = x * w; // escalamos el grid
      float vy = y * h;

      color c = pg.get(int(vx), int(vy)); // Obtenemos el color en la coordenada indicada del objeto pg
      boolean inText = (c == PGRAPHICS_COLOR); // comparamos si el color que obtenemos es igual con el que hemos rellenado el texto en pg, de esta manera sabemos las coordenadas que entran dentro del texto

      if (inText) { // Entra en el texto
        if (!continuous) { // Y toma el flag a la inversa para dibujar el punto de entrada
          noFill();
          stroke(0);
          beginShape(); // Inicia la forma
          vertex(vx, vy); //Crea un primer vertex y activa flag
          continuous = true; // al cambiar el flag a true ya no vuelve a dibujar el primer vertex
        }
        
      //curveVertex(vx,vy-random(0,10)); // ponemos valores random en la vertical de los curveCertex intermedios para probar
      
      } else { // Sale del texto
        vertex(vx, vy); // Dibuja el último vertex
        endShape(); // Cierra la forma
        continuous = false; // Y desactivamos el flag
      }
    }
  }
}
