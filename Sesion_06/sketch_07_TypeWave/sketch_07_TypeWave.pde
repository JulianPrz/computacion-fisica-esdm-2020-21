color BACKGROUND_COLOR = color(255);   // color de fondo del sketch

color PGRAPHICS_COLOR = color(0); // color de la tipografía en PGraphics
PFont f;
PGraphics pg;

void setup() {
  size(1280, 720,P2D);
  f = createFont("Liberation Sans Bold", 500);
  //print(PFont.list());
  
  pg = createGraphics(width, height); 
  pg.beginDraw();
  pg.textAlign(CENTER, CENTER);
  pg.fill(PGRAPHICS_COLOR);
  pg.textFont(f);
  pg.text("OLA", pg.width/2, pg.height/2); 
  pg.endDraw();
}

void draw(){
  background(BACKGROUND_COLOR);
  image(pg,0,0);
}
