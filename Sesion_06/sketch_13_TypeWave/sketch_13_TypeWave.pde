int gridX = 200;                       // número de puntos horizontales en el grid
int gridY = 75;                        // número de puntos verticales en el grid

float waveHeight = 10;                 // máxima altura de cada wave (vertex)
float baseHeight = 4;                  // base mínima de cada wave (vertex)

float xoff = 0;

color BACKGROUND_COLOR = color(255);   // color de fondo del sketch
color PGRAPHICS_COLOR = color(0);      // color de la tipografía en PGraphics

PFont f;

PGraphics pg;

void setup() {
  size(1280, 720, FX2D);
  f = createFont("Liberation Sans Bold", 500);
  //print(PFont.list());

  pg = createGraphics(width, height); 
  pg.beginDraw();
  pg.textAlign(CENTER, CENTER);
  pg.fill(PGRAPHICS_COLOR);
  pg.textFont(f);
  pg.text("OLA", pg.width/2, pg.height/2); 
  pg.endDraw();
}

void draw() {
  background(BACKGROUND_COLOR);
  //image(pg, 0, 0);

  float w = float(width) / gridX; 
  float h = float(height) / gridY;



  boolean continuous = false; //Flag para preguntar continuidad del siguiente punto cuando está inText

  for (int y=0; y<gridY; y++) {   // Por cada fila
    for (int x=0; x<gridX; x++) {  // Va pasando por todas las columnas

      float vx = x * w; // escalamos el grid
      float vy = y * h;

      color c = pg.get(int(vx), int(vy)); // Obtenemos el color en la coordenada indicada del objeto pg
      boolean inText = (c == PGRAPHICS_COLOR); // comparamos si el color que obtenemos es igual con el que hemos rellenado el texto en pg, de esta manera sabemos las coordenadas que entran dentro del texto

      if (inText) { // Entra en el texto
        if (!continuous) { // Y toma el flag a la inversa para dibujar el punto de entrada
          noFill();
          stroke(0);
          beginShape(); // Inicia la forma
          continuous = true; // al cambiar el flag a true ya no vuelve a dibujar el primer vertex
        }
        
        float s = sin(vx+xoff); 
        vy -= s * waveHeight + baseHeight; // restamos para que la ola se mueva hacia arriba
        curveVertex(vx, vy); // estos vertex son los que dibujan la ola
    
      } else { // Sale del texto
      
        endShape(); // Cierra la forma
        continuous = false; // Y desactivamos el flag
      }
    }
  }
  xoff+=0.01; //Incremento de tiempo para deplazarnos en la sequencia sin

}
