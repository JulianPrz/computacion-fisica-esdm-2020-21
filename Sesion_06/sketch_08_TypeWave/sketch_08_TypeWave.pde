int gridX = 200;                       // número de puntos horizontales en el grid
int gridY = 75;                        // número de puntos verticales en el grid

color BACKGROUND_COLOR = color(255);   // color de fondo del sketch
color PGRAPHICS_COLOR = color(0);      // color de la tipografía en PGraphics

PFont f;

PGraphics pg;

void setup() {
  size(1280, 720, P2D);
  f = createFont("Liberation Sans Bold", 500);
  //print(PFont.list());

  pg = createGraphics(width, height); 
  pg.beginDraw();
  pg.textAlign(CENTER, CENTER);
  pg.fill(PGRAPHICS_COLOR);
  pg.textFont(f);
  pg.text("OLA", pg.width/2, pg.height/2); 
  pg.endDraw();
}

void draw() {
  background(BACKGROUND_COLOR);
  image(pg, 0, 0);

  float w = float(width) / gridX; 
  float h = float(height) / gridY;
  //translate(w/2, h/2);

  for (int y=0; y<gridY; y++) {   // for each 'row'
    for (int x=0; x<gridX; x++) { // go over all the 'columns'

      float vx = x * w; // for each point, determine it's position in the grid
      float vy = y * h;
      line(vx, 0, vx, height); // Grid
      line(0, vy, width, vy);
      //point(vx, vy); // Grid de puntos
    }
  }
}
