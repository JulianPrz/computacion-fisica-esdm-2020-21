<< import oscP5.*;
import netP5.*;
float r,g,b;
float v1,v2,v3,v4,v5;

OscP5 oscP5;
NetAddress myRemoteLocation;

void setup() {
  size(400,400);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,12000);

}

void draw() {
  r = map(abs(v1),0,1,0,255);
  g = map(abs(v2),0,1,0,255);
  b = map(abs(v3),0,1,0,255);
  background(r,g,b);
  textSize(12);
  fill(255);
  text("r: "+r+"\ng: "+g+"\nb: "+b,width/2,height/2);
    
}

void oscEvent(OscMessage theOscMessage) {

  if(theOscMessage.checkAddrPattern("/rotationvector")==true) {

    if(theOscMessage.checkTypetag("fffff")) {

      v1 = theOscMessage.get(0).floatValue();  // get the first osc argument
      v2 = theOscMessage.get(1).floatValue(); // get the second osc argument
      v3 = theOscMessage.get(2).floatValue(); // get the third osc argument

      
      print("### received an osc message /test with typetag fff");
      println(" values: "+abs(v1)+"  "+abs(v2)+"  "+abs(v3));
      return;
    }
  }
  println("### received an osc message. with address pattern "+
          theOscMessage.addrPattern()+" typetag "+ theOscMessage.typetag());
}
