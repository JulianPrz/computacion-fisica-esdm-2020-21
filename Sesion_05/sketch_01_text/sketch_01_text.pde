//Utilizamos split() para partir el string en palabras

size(600,400);
background(0);

PFont f = createFont("FreeMono", 64);
String s = "Ola ke ase?";

String[] words = split(s, " ");
//printArray(PFont.list());

textFont(f);
textSize(64);
//textAlign(CENTER);
text(s,10,100);

float x = 10;
for (int i=0; i < words.length; i++){
  //char c = s.charAt(i);
  textSize(64);
  fill(random(25,255));
  text(words[i], x, 300);
  //x += textWidth(words[i]+1); //Espaciado por pt
  x += textWidth(words[i])+40; //Espaciado por pixeles
}
