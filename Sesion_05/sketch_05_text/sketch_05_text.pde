String[] words;
String msg;

int nwords = 100;
float [] x = new float [nwords];
float [] y = new float [nwords];
float [] a = new float [nwords];


void setup() {
  size(600, 400);
  //frameRate(5);
  //background(0);
  String[] lines = loadStrings("quijote.txt");
  String entireText = join(lines, " ");
  //printArray(entireText);
  words = splitTokens(entireText, ",.?!: ");
  textAlign(LEFT, CENTER);
  for (int n = 0; n< nwords; n++) {
    x[n] = random(width);
    y[n] = random(height);
    a[n] = random(TWO_PI);
  }
}

void draw() {
  background(0);
  //fill(255);
  textSize(14);
  //textAlign(CENTER);


  //textAlign(CENTER, CENTER);
  for (int n = 0; n< nwords; n++) {
    msg = words[n]; 

    //float s = map(n, 0, nwords, 0, 255);
    float s = map(sin(a[n]), -1, 1, 0, 255);
    fill(255,s);
    text(msg, x[n], y[n]);

    a[n] += 0.01;

    //float v = 0.2 + msg.length()/4;
    x[n] += 0.2;
    if (x[n] > width) {
      x[n] = 0;
    }
  }
}
