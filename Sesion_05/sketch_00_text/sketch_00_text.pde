//Mostramos texto en canvas, listado de fuentes instaladas, creamos un segundo texto en el que modificamos caracteres individualmente

size(600,400);
background(0);

PFont f = createFont("FreeMono", 64);
String s = "Ola ke ase?";

//printArray(PFont.list());

textFont(f);
textSize(64);
//textAlign(CENTER);
text(s,10,100);

float x = 10;
for (int i=0; i < s.length(); i++){
  char c = s.charAt(i);
  //textSize(random(32,100));
  fill(random(25,255));
  text(c, x, 300);
  x += textWidth(c);
}
