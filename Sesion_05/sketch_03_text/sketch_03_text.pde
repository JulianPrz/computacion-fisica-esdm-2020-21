// Cargamos un archivo .txt y lo dividimos en líneas de texto. Después unimos todas las lineas en un único string de palabras,
// y después creamos un array de todas las palabras separadas para mostrarlas en pantalla individualmente

String[] words;
int index = 0;

void setup() {
  size(600, 400);
  frameRate(5);
  background(0);
  String[] lines = loadStrings("quijote.txt");
  //printArray(lines);
  String entireText = join(lines, " ");
  //print(entireText);
  words = splitTokens(entireText, ",.?!:; ");
  //printArray(words);
}

void draw() {
  background(0);
  fill(255);
  textSize(64);
  textAlign(CENTER);
  text(words[index],width/2, height/2);
  index++;
}
