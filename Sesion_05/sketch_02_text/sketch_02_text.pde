//Utilizamos split() para partir el string en palabras e int() para pasar tipo de datos de string a enteros

size(600,400);
background(0);

PFont f = createFont("FreeMono", 32);
String s = "100,50,25,75,14";

String[] nums = split(s, ","); //Esta sería la forma de separar valores de un archivo CSV (Comma Separated Values)

int[] vals = int(nums);

textFont(f);
textAlign(CENTER,CENTER);
noStroke();

translate(100,0);
for (int i=0; i < vals.length; i++){
  ellipse(i*100,height/2,vals[i],vals[i]);
  //rect(i*100-10,height/2,20,-vals[i]);
  text(nums[i], i*100,height*3/4); //Podemos utilizar en array nums de tipo String para mostrar los valores como texto
}
