import processing.video.*; //Importamos la librería de video

Movie video; //Utilizamos el objeto Movie que nos permite cargarle un archivo de video
int x = -8; //empezamos en -1 frame ya que el primero no le da tiempo a visualizarlo
int y, f;

void setup() {
  size(480, 372);

  video = new Movie(this, "video.mp4");
  video.play();
  video.volume(0); //muteamos el video
  println(video.duration());
  println();
}

void movieEvent(Movie video) { 
  video.read();               
}

void draw() {
  video.jump(f++); //salta al frame 60 del video cada frame del programa
  if (x>=width) {
    x=0;
    y=y+6;
  }
  image(video, x, y, 8, 6);
  x=x+8;
  println("x= "+x, "y= "+y);
  //Captura imagen y cierre de programa
  if(y>=height){
    saveFrame("cinemaRedux.png");
    println("Captura guardada, saliendo del programa...");
    exit();
  }
  
}
