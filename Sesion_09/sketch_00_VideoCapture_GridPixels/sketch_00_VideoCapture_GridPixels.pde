import processing.video.*; //Importamos la librería de video

Capture video;

int gridSize = 5; // variable del tamaño del celda
int gap = 2; // variable del espacio entre celdas

void settings() {
  video = new Capture(this, 640, 480, "HD Pro Webcam C920", 30);
  size(video.width, video.height); // Tamaño del canvas adaptado al tamaño de la imagen
}

void setup() {
  noStroke();
  video.start();
}

void captureEvent(Capture video){ //Evento que se ejecuta cada vez que hay un frame nuevo
  video.read();                    //Lee el frame actual
}

void draw() {
  background(12, 34, 52); // esto le mete un color de fondo

  gridSize = int(map (mouseX, 0, width, 1, 20)); //mapeamos valores mouseX con el ancho de la celda de retícula
  gap = int(map (mouseY, 0, height, 1, 20)); //mapeamos valores mouseY con el tamaño del hueco entre celdas


  for (int i=0; i<video.width; i++) {
    for (int j=0; j<video.height; j++) {
      color c = video.get(i*(gridSize+gap), j*(gridSize+gap));
      int x = i*(gridSize+gap);
      int y = j*(gridSize+gap);
      fill(c);
      rect(x, y, gridSize, gridSize);
    }
  }
}
