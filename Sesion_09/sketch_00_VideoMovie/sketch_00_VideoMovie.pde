import processing.video.*; //Importamos la librería de video

Movie video; //Utilizamos el objeto Movie que nos permite cargarle un archivo de video

void setup() {
  size(425, 240);
  
  video = new Movie(this, "big_buck_bunny.mp4"); //Inicializamos el objeto video con los sig parámetros (this, String del nombre de dispositivo de captura que nos aparezca en Capture.list(), ancho, alto, framesPerSecond)
  //video = new Capture(this, Capture.list()[0]); //Esta es otra forma de llamar a las diferentes 
  
  video.loop(); //Iniciamos el video
}

//Metodo 1: este es mejor que el método 2. Ya que procesa el video fuera del draw
void movieEvent(Movie video){ //Evento que se ejecuta cada vez que hay un frame nuevo
  video.read();                    //Lee el frame actual
}

void mousePressed(){
  video.jump(int(random(video.duration())));
}
void draw() {
  //Método 2
  //if (video.available()) { //Paso previo para comprobar si la cámara está operativa
  //  video.read(); //Para obtener los pixeles en el momento concreto, un refresh de la imagen
  //}
  //float r = map(mouseX,0,width,0,4);
  //video.speed(r);
  video.jump(int(random(video.duration())));
  background(0);
  image(video, 0, 0);
}
