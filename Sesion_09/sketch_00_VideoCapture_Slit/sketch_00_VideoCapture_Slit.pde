import processing.video.*; //Importamos la librería de video

Capture video; //Utilizamos el objeto Capture que nos permite utilizar la webcam de nuestro ordenador y le llamamos video
float inc=0;
void setup() {
  size(640, 240);
  printArray(Capture.list()); //Imprimimos la lista de las fuentes de captura de video disponibles
  video = new Capture(this, 320, 240, "HD Pro Webcam C920", 30); //Inicializamos el objeto video con los sig parámetros (this, String del nombre de dispositivo de captura que nos aparezca en Capture.list(), ancho, alto, framesPerSecond)
  //video = new Capture(this, Capture.list()[0]); //Esta es otra forma de llamar a las diferentes 
  
  video.start(); //Iniciamos el video
  background(0);
}

void captureEvent(Capture video){ //Evento que se ejecuta cada vez que hay un frame nuevo
  video.read();                    //Lee el frame actual
}

void draw() {
  if(inc>=width){
    inc=0;
  }
  copy(video,video.width/2,0,2,height,(int)inc,0,2,height);
  inc+=0.5;
}
