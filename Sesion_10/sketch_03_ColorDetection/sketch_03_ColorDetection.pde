// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/nCVZHROb_dE

import processing.video.*;

Capture video;

color trackColor; 
float threshold = 25;

float r2, g2, b2;

void setup() {
  size(640, 480);
  //printArray(Capture.list());
  video = new Capture(this, Capture.list()[0]); 
  video.start();
  trackColor = color(255, 0, 0);
}

void captureEvent(Capture video) {
  video.read();
}

void draw() {
  video.loadPixels();
  image(video, 0, 0);

  threshold = map(mouseX, 0, width, 0, 100);
  //threshold = 80;

  float avgX = 0;
  float avgY = 0;

  int count = 0;

  // Begin loop to walk through every pixel
  for (int x = 0; x < video.width; x++ ) {
    for (int y = 0; y < video.height; y++ ) {
      int loc = x + y * video.width;
      // What is current color
      color currentColor = video.pixels[loc];
      float r1 = red(currentColor);
      float g1 = green(currentColor);
      float b1 = blue(currentColor);
      r2 = red(trackColor);
      g2 = green(trackColor);
      b2 = blue(trackColor);


      float d = dist(r1, g1, b1, r2, g2, b2); 

      if (d < threshold) { // Ya no es el pixel más similar, sino los pixeles lo suficientemente similares al color indicado: umbral de similitud (threshold)
        stroke(255);
        strokeWeight(1);
        point(x, y); // Muestrame los pixeles que entran dentro del umbral

        avgX += x; // Suma las coordenadas en X de todos los puntos de color dentro del umbral. Ej.: 20,21,22,23
        avgY += y; // lo mismo para eje Y
        count++; // Va aumentando la cuenta en funcion de los pixeles próximos, para luego poder hacer la media
      }
    }
  }

  if (count > 0) { 
    avgX = avgX / count; // Ej.: 20,21,22,23= 87/4= 21.75
    avgY = avgY / count;

    fill(r2, g2, b2);
    strokeWeight(4.0);
    stroke(0);
    ellipse(avgX, avgY, 24, 24);
  }
}

void mousePressed() {
  int loc = mouseX + mouseY*video.width;
  trackColor = video.pixels[loc];
}
