//El ejemplo "BackgroundSubstraction" que viene en la librería da error
//Aquí en workaround: https://stackoverflow.com/questions/37662245/processing-opencv-and-ipcapture-library-error-width0-and-height0-cannot-be

import gab.opencv.*;
import processing.video.*;

Movie video;
OpenCV opencv;

void setup() {
  size(720, 480);
  video = new Movie(this,"video.mp4");
  opencv = new OpenCV(this, 720, 480);
  
  opencv.startBackgroundSubtraction(5, 3, 0.5);
  
  video.loop();
  //video.play();

}

void draw() {
  //image(video, 0, 0);  
    background(0);
  if(video.width > 0 && video.height > 0){//check if the cam instance has loaded pixels
      opencv.loadImage(video);
  }
      
  //opencv.loadImage(video);
  
  opencv.updateBackground();
  
  opencv.dilate();
  opencv.erode();

  noFill();
  stroke(255, 0, 0);
  strokeWeight(1);
  for (Contour contour : opencv.findContours()) {
    contour.draw();
  }
}

void movieEvent(Movie m) {
  m.read();
}
