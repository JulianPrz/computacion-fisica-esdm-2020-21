import processing.video.*;
import gab.opencv.*;
import java.awt.Rectangle;

Capture video;
OpenCV opencv;
Rectangle[] faces;

void setup() {
  size(320, 180);
  video = new Capture(this,Capture.list()[2]);
  video.start();  

  opencv = new OpenCV(this, video.width, video.height);  
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
}

void draw() {
  opencv.loadImage(video);
  faces = opencv.detect();

  image(opencv.getInput(), 0, 0);

  noFill();
  stroke(0, 255, 0);
  strokeWeight(3);
  for (int i = 0; i < faces.length; i++) {
    rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
  }
}

void captureEvent(Capture video) {
  video.read();
}
