import processing.video.*;

Capture video;

color trackColor; //Esta variable almacena el color que indiquemos cuando clickemos en la imagen del video

boolean showVideo = true;

void setup() {
  size(640, 480);
  //printArray(Capture.list());
  video = new Capture(this, Capture.list()[0]);
  video.start();
  
  trackColor = color(255, 0, 0); //Comienza trackeando el color rojo para poder comenzar a comparar
}

void captureEvent(Capture video) {
  video.read();
}

void draw() {
  video.loadPixels(); //Carga la informacion al array pixels[]
  if(showVideo){
  image(video, 0, 0);
  }else{
    //background(0);
  }
  // Before we begin searching, the "world record" for closest color is set to a high number that is easy for the first pixel to beat.
  float worldRecord = 500;

  // coordenadas que utilizaremos para el pixel con el color más próximo al indicado con trackColor
  int closestX = 0;
  int closestY = 0;

  // Comenzamos con el bucle for anidado para pasar por todos lo píxeles
  for (int x = 0; x < video.width; x ++ ) {
    for (int y = 0; y < video.height; y ++ ) {
      int loc = x + y*video.width;
      // Cual es el color actual de cada píxel
      color currentColor = video.pixels[loc];
      //Color actual
      float r1 = red(currentColor);
      float g1 = green(currentColor);
      float b1 = blue(currentColor);
      //Color a trackear
      float r2 = red(trackColor);
      float g2 = green(trackColor);
      float b2 = blue(trackColor);

      // Usamos dist() para comparar ambos colores
      // R    G    B
      // 200, 50 , 200
      // 0  , 200, 100
      //---------------
      // 200, 150, 100 = 450
      
      float d = dist(r1, g1, b1, r2, g2, b2);
 
 
      // Si hay un pixel que tiene una distacia menor que la marca de worldRecord, guarda/actualiza esa nueva marca y también las coordenadas XY
      // De esta manera siempre tendremos el píxel más próximo detectado en toda nuestra imagen
      if (d < worldRecord) {
        worldRecord = d;
        closestX = x;
        closestY = y;
      }
    }
  }
  
  // Solo consideramos el color encontrado si la distancia de color es menos de 10. Este valor es arbitrario y lo podemos cambiar para ajustar
  println(worldRecord);
  if (worldRecord < 10) { 
    // Draw a circle at the tracked pixel
    fill(trackColor);
    strokeWeight(4.0);
    stroke(0);
    ellipse(closestX, closestY, 16, 16);
  }
}

void mousePressed() {
  // Guarda el color en la variable trackcolor de donde hagamos click en la imagen
  int loc = mouseX + mouseY*video.width;
  trackColor = video.pixels[loc];
}

void keyPressed(){
  showVideo = !showVideo;
  background(0);
}
