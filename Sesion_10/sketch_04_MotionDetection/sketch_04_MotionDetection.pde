import processing.video.*;

Capture video;
PImage prev;

float threshold = 25;

float motionX = 0;
float motionY = 0;

float lerpX = 0;
float lerpY = 0;

void setup() {
  size(640, 480);
  //printArray(Capture.list());
  video = new Capture(this, Capture.list()[2]); 
  video.start();
  prev = createImage(width, height, RGB);
}

//void mousePressed(){
//    prev.copy(video, 0, 0, video.width, video.height, 0, 0, prev.width, prev.height);
//  prev.updatePixels();
//}

void captureEvent(Capture video) {
  prev.copy(video, 0, 0, video.width, video.height, 0, 0, prev.width, prev.height);
  prev.updatePixels();
  video.read();
}

void draw() {
  video.loadPixels();
  prev.loadPixels();
  image(video, 0, 0);

  //threshold = map(mouseX, 0, width, 0, 100);
  threshold = 50;

  float avgX = 0;
  float avgY = 0;
  
  int count = 0;

  loadPixels();
  // Begin loop to walk through every pixel
  for (int x = 0; x < video.width; x++ ) {
    for (int y = 0; y < video.height; y++ ) {
      int loc = x + y * video.width;
      // What is current color
      color currentColor = video.pixels[loc];
      float r1 = red(currentColor);
      float g1 = green(currentColor);
      float b1 = blue(currentColor);
      color prevColor = prev.pixels[loc];
      float r2 = red(prevColor);
      float g2 = green(prevColor);
      float b2 = blue(prevColor);

      float d = dist(r1, g1, b1, r2, g2, b2); 

      if (d > threshold) { // Ya no es el pixel más similar, sino los pixeles lo suficientemente similares al color indicado: umbral de similitud (threshold)
        //stroke(255);
        //strokeWeight(1);
        //point(x, y); // Muestrame los pixeles que entran dentro del umbral
        avgX += x;
        avgY += y;
        count++;

        pixels[loc] = color(255);
      } else {
        pixels[loc] = color(0);
      }
    }
  }
  updatePixels();

  image(video, 0,0,100,80);
  //image(prev, 100,0,100,80);

  if (count > 200) { 
    motionX = avgX / count;
    motionY = avgY / count;
    
    lerpX = lerp(lerpX,motionX,0.1);
    lerpY = lerp(lerpY,motionY,0.1);
    
    fill(255, 0, 0);
    strokeWeight(2.0);
    stroke(0);
    ellipse(lerpX, lerpY, 24, 24);
  }
}
