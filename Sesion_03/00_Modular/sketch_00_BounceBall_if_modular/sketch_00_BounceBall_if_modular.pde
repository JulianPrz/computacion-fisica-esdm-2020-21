int rad = 30;        
float xpos, ypos;

float xspeed = 2.8;  
float yspeed = 2.2; 

int xdirection = 1;  
int ydirection = 1;

void setup() 
{
  size(640, 360);
  noStroke();
  ellipseMode(RADIUS);

  xpos = width/2;
  ypos = height/2;
}

void draw() {
  background(102);
  muestraBola();
  mueveBola();
  tocaBorde();
}

void muestraBola() {
  ellipse(xpos, ypos, rad, rad);
}

void mueveBola(){ 
  xpos = xpos + ( xspeed * xdirection );
  ypos = ypos + ( yspeed * ydirection );
}

void tocaBorde(){
  if (xpos > width-rad || xpos < rad) {
    xdirection *= -1;
  }
  if (ypos > height-rad || ypos < rad) {
    ydirection *= -1;
  }
}
