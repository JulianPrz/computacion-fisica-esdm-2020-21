float tx1=1;
float ty1=200;
float tr=300;

void setup() {
  size(300,300);
  background(0);
}

void draw() {
  
  float x1 = noise(tx1);
  float y1 = noise(ty1);
  float r = noise(tr);

  x1 = map(x1, 0, 1, 0, width);
  y1 = map(y1, 0, 1, 0, height);
  r = map(r, 0, 1, 0, 20);
 
  

  stroke(255);
  ellipse(x1,y1,r,r);
  tx1+=0.005;
  ty1+=0.005;
  tr+=0.02;

}
