float tx1=1;
float ty1=1000;

void setup() {
  size(300,300);
}

void draw() {
  background(0);
  float x1 = noise(tx1);
  float y1 = noise(ty1);

  x1 = map(x1, 0, 1, 0, width);
  y1 = map(y1, 0, 1, 0, height);

  stroke(255);
  ellipse(x1,y1,20,20);
  tx1+=0.005;
  ty1+=0.005;
}
