///1) while(). Se ejecuta mientras se cumpla lo que aparezca entre paréntesis
int i = 0;

while (i < 80) {
  line(30, i, 80, i);
  i = i + 5;
}

//2) while es una función que mientras se cumpla no parará, por lo que si lo que
//comprueba tarda mucho en ser "false" no se ejecutará el resto de nuestro código

//int i = 0;

//void setup(){
//  size(400,400);
//}

//void draw(){
//  ellipse(mouseX,mouseY,20,20);

//  while (i < 99999999) {
//    line(30, i, 80, i);
//    i = i + 5;
//  } 
//}

//3)Dependiendo de nuestro programa podemos evitar que while se quede en un bucle infinito
//con "break"

//int i = 0;

//void setup(){
//  size(400,400);
//}

//void draw(){
//  ellipse(mouseX,mouseY,20,20);

//  while (i < 99999999) {
//    if(i == 300){
//    line(30, i, 80, i);
//    break;
//    }
//    i = i + 5;
//  } 
//}
