//Ejemplo para ver funcion "if else". De esta manera podemos controlar estados, o utilizar,
//más comúnmente conocido en programación, "flags"

boolean state = false;
int colorState = 0;

void setup(){
}

void draw(){
  if (state){           //si es true, ejecuta esto
    colorState = 0;
  }else{                //y si no, si es false, ejecuta esto otro
    colorState = 255;
  }
  background(colorState);
}
void mousePressed(){
 state = !state;        //cambiamos el state a su valor opuesto cada vez que clickamos
}
