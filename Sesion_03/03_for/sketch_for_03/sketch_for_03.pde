int w = 2;

void setup() {
  size(600, 600);
}

void draw() {
  w = int(map(mouseX,0,width,1,50));
  for (int i = 0; i < width; i = i+w) {
    for (int j = 0; j < height; j = j+w) {
      fill(random(255));
      noStroke();
      rect(i, j,w,w);
    }
  }
}
