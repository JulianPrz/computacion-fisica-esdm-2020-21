/* 
Utilizamos for() para itinerar en incrementos de 20 
hasta llegar al valor de width. Comenzamos con lineas 
horizontales hacia abajo de 0 hasta la mitad del canvas
y cuando i es mayor que height, comienzan a dibujarse en
vertical hacia la derecha a partir de la mitad del canvas
*/

int gap = 20;
void setup(){
size(400,200);
background(0);
stroke(255);
for(int i = gap;i<width;i+=gap){
  line(gap,i,width/2-gap,i);
  if(i >= height){
     line(i,gap,i,height-gap);
  }
}
}

void draw(){
}
