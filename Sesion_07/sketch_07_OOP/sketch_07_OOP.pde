Ball[] b = new Ball[20]; //creamos una lista de 20 objetos que se llama b
int nBalls = 4;

void setup() {
  size(600, 600);
  for (int i = 0; i < b.length; i++) {
    b[i] = new Ball(random(width), random(height), 15, color(255)); //Nuevos objetos Ball con argumentos
  }
}

void mousePressed(){
  nBalls++; //sumamos bolas cuando clickamos
}

void keyPressed(){
  nBalls--; //restamos bolas cuando tecleamos
}

void draw() {
  background(0);
  for (int i = 0; i < nBalls; i++) { // b.length es el tamaño de nuestra lista
    b[i].display();
    b[i].move();
    b[i].checkEdges();
  }
}
