float noiseVal;
float noiseScale=0.02;

void setup() {
  size(200,200);
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      noiseDetail(8);
      noiseVal = noise(x * noiseScale, y * noiseScale);
      stroke(noiseVal*255);
      point(x,y);
    }
  }
}
