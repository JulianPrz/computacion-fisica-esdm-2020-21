Ball b1,b2; //declaramos el nombre del objeto que utiliza la clase Ball

void setup(){
  size(600,600);
  
  b1 = new Ball(15); //Nuevo objeto Ball enviando un argumento
  b2 = new Ball(40); //creamos un segundo objeto Ball enviando un argumento
}


void draw(){
  background(0);
  b1.display(); //llamamos a la función display() del objeto
  b2.display();
}
