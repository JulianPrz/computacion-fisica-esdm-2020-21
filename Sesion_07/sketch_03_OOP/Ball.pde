class Ball {
  // variables propias del objeto
  float x, y;
  float w;

  //Constructor
  Ball(float _w) { //Recibimos el argumento para construir un objeto con ese valor
    x = random(width);
    y = random(height);
    w = _w; //y lo pasamos a la variable w de la clase Ball
  }

  void display() {
    fill(255);
    ellipse(x, y, w, w);
  }
}
