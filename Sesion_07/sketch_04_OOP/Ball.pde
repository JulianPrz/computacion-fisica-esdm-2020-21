class Ball {
  // variables propias del objeto
  float x, y, w;
  color c;

  //Constructor
  Ball(float _x, float _y, float _w, color _c) {
    x = _x;
    y = _y;
    w = _w;
    c = _c;
  }

  void display() {
    fill(c);
    ellipse(x, y, w, w);
  }
}
