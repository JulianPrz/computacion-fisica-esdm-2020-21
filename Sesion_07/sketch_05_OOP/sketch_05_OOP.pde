Ball b; //declaramos el nombre del objeto que utiliza la clase Ball

void setup(){
  size(600,600);
  
  b = new Ball(random(width), random(height), 15, color(255)); //Nuevo objeto Ball con argumentos
}


void draw(){
  background(0);
  b.display();
  b.move();
  b.checkEdges();
}
