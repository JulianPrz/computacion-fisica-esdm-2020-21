float angle = 0;
float x;
float r = 100;
float inc = 0.1;

void setup() {
  size(640, 360);
  noStroke();
  background(0);
}

void draw() {
  
  fill(255, 204, 0);
  ellipse(x, height/2+(r*sin(angle)), 10, 10);
  //fill(255);
  //text("Sin: "+sin(angle), 15, height*3/4, 64);
  //text("Diameter: "+d, 15, height*3/4+15, 64);
  //text("Angle: "+angle, 15, height*3/4+30, 64);
  angle += inc;
  x++;
  if (angle >= TWO_PI) {
    angle=0;
  }
  if(x>=width){
    background(0);
    x = 0;
    inc = random(0.01,0.1);
    r = random(10,200);
  }
}
