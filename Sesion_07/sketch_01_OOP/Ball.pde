class Ball {
  // variables propias del objeto
  float x, y;
  float w = 15;

  //Constructor
  Ball() {
    x = random(width);
    y = random(height);
  }

  void display() {
    fill(255);
    ellipse(x, y, w, w);
  }
}
