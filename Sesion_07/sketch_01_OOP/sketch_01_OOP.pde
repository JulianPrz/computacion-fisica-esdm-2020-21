Ball b; //declaramos el nombre del objeto que utiliza la clase Ball

void setup(){
  size(600,600);
  
  b = new Ball(); //Nuevo objeto Ball
  
}


void draw(){
  background(0);
  b.display(); //llamamos a la función display() del objeto
}
