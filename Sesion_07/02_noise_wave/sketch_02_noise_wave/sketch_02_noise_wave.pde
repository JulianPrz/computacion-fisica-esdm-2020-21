float x;
float tx;

void setup() {
  size(640, 360);
  noStroke();
  background(0);
}

void draw() {

  fill(255, 204, 0);
  ellipse(x, height*noise(tx), 5, 5);
  fill(255);
  tx += 0.01;
  x++;

  if (x>=width) {
    background(0);
    x = 0;
  }
}
