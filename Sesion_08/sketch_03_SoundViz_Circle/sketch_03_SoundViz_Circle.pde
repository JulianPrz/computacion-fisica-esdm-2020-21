
import ddf.minim.analysis.*;
import ddf.minim.*;

Minim minim;
AudioPlayer player;
BeatDetect beat;
FFT fft;
float r;


void setup()
{
  size(500, 500);
  colorMode(HSB);
  noCursor();

  minim = new Minim(this);
  player = minim.loadFile("1-04 Saturnz Barz.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());

  beat = new BeatDetect();
}

void draw()
{
  player.play();
  background(0);
  beat.detect(player.mix);
  if (beat.isOnset()){
     background(255); 
  }
  strokeWeight(4);

  translate(width/2, height/2);
  fft.forward(player.mix);

  for (int i = 0; i < fft.specSize()/2; i+=6) {
    beginShape();
    float angle = map(i, 0, fft.specSize()/2, 0, 360); 
    float amp = fft.getBand(i)*2;
    float r = map(amp, 0, fft.specSize()/16, 60, 120);
    float x = cos(radians(angle))*r;
    float y = sin(radians(angle))*r;
    stroke(i, 255, 255);
    vertex(x, y);
    endShape();
  }
}
