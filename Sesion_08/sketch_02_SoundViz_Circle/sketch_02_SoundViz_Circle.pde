
import ddf.minim.analysis.*;
import ddf.minim.*;

Minim minim;
AudioPlayer player;
FFT fft;


void setup()
{
  size(500, 500);
  noCursor();
  rectMode(CENTER);


  minim = new Minim(this);
  player = minim.loadFile("I Got You.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

void draw()
{
  player.play();
  background(0);
  strokeWeight(4);
  stroke(255,180);
  translate(width/2, height/2);
  fft.forward(player.mix);
  //512 values below --> this loop is called 25 times per second
  //try getting the highest value and making a shape based on that value
  //maybe change color based on the size of the value?

  for (int i = 0; i < fft.specSize(); i++) {
      pushMatrix();
      if(i >= 0 && i <= 120){
      rotate(radians(i*3));
      }
      //float x = cos(angle) * 100;
      //float y = sin(angle) * 100;
      line(0, 100, 0, 100+fft.getBand(i)*2);
      //rect(i, height/2, 2, fft.getBand(i)*10); //this is float which is very much like a double
      popMatrix();
    }
  }
