ArrayList <Ball> balls; //creamos una lista de tamaño flexible que se llama balls

import ddf.minim.analysis.*;
import ddf.minim.*;

Minim minim;
AudioPlayer player;
BeatDetect beat;
FFT fft;
float r = 4;


void setup()
{
  size(500, 500);
  balls = new ArrayList();
  colorMode(HSB);
  noCursor();

  minim = new Minim(this);
  player = minim.loadFile("1-04 Saturnz Barz.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());

  beat = new BeatDetect();
}

void draw()
{
  player.play();
  background(0);
  beat.detect(player.mix);
  if (beat.isOnset()){
     Ball b = new Ball(width/2, height/2, random(5,10), color(fft.getBand(int(random(100))),255,255), 255); //Crea una nueva bola
      balls.add(b); //Añádela a la lista
}
 for (int i = 0; i < balls.size(); i++) { // iteramos en función del tamaño de nuestra lista para mostrar todas las bolas
    Ball b = balls.get(i); //Llama a cada bola de nuestra lista
    b.display(); 
    b.move();
    if (b.isDead()){
      balls.remove(i);
    }
  }
  strokeWeight(4);

  translate(width/2, height/2);
  fft.forward(player.mix);
  beginShape();
  for (int i = 0; i < fft.specSize()/2; i+=6) {
    
    float angle = map(i, 0, fft.specSize()/2, 0, 360); 
    float amp = fft.getBand(i)*2;
    float r = map(amp, 0, fft.specSize()/16, 60, 120);
    //fill(i,255,255);
    float x = cos(radians(angle))*r;
    float y = sin(radians(angle))*r;
    stroke(255);
    noFill();
    curveVertex(x, y);
    //line(cos(radians(angle))*20,sin(radians(angle))*20,x,y);
    
  }
  endShape(CLOSE);
  println(balls.size());
}
