class Ball {
  // variables propias del objeto
  float x, y, w, lifespan;
  color c;

  float vx, vy; //variables para velocidad

  //Constructor
  Ball(float _x, float _y, float _w, color _c, float _lifespan) {
    x = _x;
    y = _y;
    w = _w;
    c = _c;
    lifespan = _lifespan;

    vx = random(-3,3);
    vy = random(-3,3);
  }

  void display() {
    lifespan-=2;
    fill(c,lifespan);
    noStroke();
    ellipse(x, y, w, w);
  }

  void move() {
    x += vx;
    y += vy;
  }
  
  boolean isDead(){
    if(lifespan <=0){
      return true;
    } else {
      return false;
    }
  }


}
