import ddf.minim.analysis.*;
import ddf.minim.*;

Minim       minim;
AudioPlayer player;
FFT         fft;

void setup(){
  size(512, 256);
  noCursor();
  rectMode(CENTER);

  minim = new Minim(this);
  player = minim.loadFile("I Got You.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

void draw(){
  background(0);
  player.play();
  fft.forward(player.mix);
  
  stroke(255);
  for(int i = 0; i <= fft.specSize();i++){
    line(i*2,height,i*2,height-fft.getBand(i));
  }
}
