import ddf.minim.analysis.*;
import ddf.minim.*;

Minim       minim;
AudioInput  in;
FFT         fft;


void setup()
{
  size(512, 256);
  noCursor();
  rectMode(CENTER);


  minim = new Minim(this);
  in = minim.getLineIn();
  fft = new FFT(in.bufferSize(), in.sampleRate());
}

void draw()
{
  background(0);
  fft.forward(in.mix);

  stroke(255);
  for (int i = 0; i <= fft.specSize(); i++) {
    line(i*2, height, i*2, height-fft.getBand(i)*4);
  }
}
