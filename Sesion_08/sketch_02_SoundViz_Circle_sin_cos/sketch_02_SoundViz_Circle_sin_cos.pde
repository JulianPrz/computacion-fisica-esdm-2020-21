
import ddf.minim.analysis.*;
import ddf.minim.*;

Minim minim;
AudioPlayer player;
FFT fft;
float r=100;


void setup()
{
  size(500, 500);
  noCursor();
  minim = new Minim(this);
  player = minim.loadFile("I Got You.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

void draw()
{
  player.play();
  background(0);
  strokeWeight(4);
  stroke(255, 180);
  translate(width/2, height/2);
  fft.forward(player.mix);

  for (int i = 0; i < fft.specSize(); i++) {
    if (i >= 0 && i <= 120) {
      line(cos(radians(i*3))*r, sin(radians(i*3))*r, cos(radians(i*3))*(r+fft.getBand(i)), sin(radians(i*3))*(r+fft.getBand(i)));
    }
  }
}
