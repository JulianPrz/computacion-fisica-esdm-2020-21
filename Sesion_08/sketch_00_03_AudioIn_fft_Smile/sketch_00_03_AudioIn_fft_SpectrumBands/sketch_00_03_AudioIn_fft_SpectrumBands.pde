import ddf.minim.analysis.*;
import ddf.minim.*;

Minim         minim;
AudioPlayer   player;
AudioMetaData metadata;
BeatDetect    beat;
FFT           fft1, fft2;
//PFont f;


void setup()
{
  size(512, 512);
  noCursor();

  minim = new Minim(this);
  beat = new BeatDetect();
  player = minim.loadFile("04. Who.mp3");
  metadata = player.getMetaData();
  fft1 = new FFT(player.bufferSize(), player.sampleRate());
  fft2 = new FFT(player.bufferSize(), player.sampleRate());
}

void draw()
{
  background(0);
  text("Título: "+metadata.title(), 100, 100);
  text("Álbum: "+metadata.album(), 100, 120);
  text("Artista: "+metadata.author(), 100, 140);
  player.play();
  beat.detect(player.mix);
  if(beat.isOnset()){
    background(255);
  }
  
  fft1.forward(player.left);
  fft2.forward(player.right);

  stroke(255, 0, 0);
  for (int i = 0; i <= fft1.specSize(); i++) {
    line(i*2, height/2, i*2, height/2-fft1.getBand(i));
  }

  stroke(0, 255, 0);
  for (int i = 0; i <= fft2.specSize(); i++) {
    line(i*2, height/2, i*2, height/2+fft2.getBand(i));
  }
  
    noStroke();
  fill(0, 127, 0);
  rect( width*0.6, height*0.15, player.right.level()*width*0.2, height*0.02 );
  stroke(0, 127, 0);
  noFill();
  rect( width*0.6, height*0.15, width*0.2, height*0.02 );

}
