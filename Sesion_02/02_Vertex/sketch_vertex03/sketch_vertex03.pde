//Creamos polígonos random con 4 vértices cuando pulsemos una tecla
//Creamos variables para almacenar las posiciones
//Utilizamos array para acortar código y para elegir una lista de colores
//Usamos KeyPressed para exportar frame

float[] x = new float[4];
float[] y = new float[4];
int colors[] = {#DB7654, #893D60, #D6241E, #F2AC2A, #3D71B7, #85749D, #21232E, #5FA25A, #5D8EB4};

void setup() {
  size(800, 800);
  background(0);
  generate();
}

void draw() {
  //background(0);
  noStroke();
  beginShape();
  for (int i = 0; i<4; i++) {
    vertex(mouseX+x[i], mouseY+y[i]);
  }
  endShape(CLOSE);
}

void mousePressed() {
  //background(0);
  generate();
}
void generate() {
  int r = round(random(8));
  fill(colors[r]);
  translate(mouseX, mouseY);
  for (int i = 0; i<4; i++) {
    vertex(x[i]=random(-width/4, width/4), y[i]=random(-height/4, height/4));
  }
}

void keyPressed(){
  if(key=='s'){
    saveFrame("dibujo-######.png");
    println("Dibujo guardado");
  }
}
