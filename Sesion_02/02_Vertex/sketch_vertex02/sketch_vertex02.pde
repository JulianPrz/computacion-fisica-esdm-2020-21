//Creamos polígonos random con 4 vértices cuando pulsemos una tecla
//Creamos variables para almacenar las posiciones
//Utilizamos array para acortar código

float[] x = new float[4];
float[] y = new float[4];

void setup() {
  size(800, 800);
  background(255);
  generate();
}

void draw() {
  background(255);
  beginShape();
  for (int i = 0; i<4; i++) {
    vertex(mouseX+x[i], mouseY+y[i]);
  }
  endShape(CLOSE);
}

void mousePressed() {
  background(255);
  generate();
}

void generate() {
  translate(mouseX, mouseY);
  for (int i = 0; i<4; i++) {
    vertex(x[i]=random(-width/4, width/4), y[i]=random(-height/4, height/4));
  }
}
