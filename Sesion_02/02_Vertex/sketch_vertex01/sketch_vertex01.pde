//Creamos polígonos random con 4 vértices cuando pulsemos una tecla
//Creamos variables para almacenar las posiciones

float x1, y1, x2, y2, x3, y3, x4, y4;

void setup() {
  size(800, 800);
  background(255);
  generate();
}

void draw() {
  background(255);
  beginShape();
  vertex(mouseX+x1, mouseY+y1);
  vertex(mouseX+x2, mouseY+y2);
  vertex(mouseX+x3, mouseY+y3);
  vertex(mouseX+x4, mouseY+y4);
  endShape(CLOSE);
}

void mousePressed() {
  background(255);
  generate();
}
void generate() {
  translate(mouseX, mouseY);
  x1=random(-width/4, width/4);
  y1=random(-height/4, height/4);
  x2=random(-width/4, width/4);
  y2=random(-height/4, height/4);
  x3=random(-width/4, width/4);
  y3=random(-height/4, height/4);
  x4=random(-width/4, width/4);
  y4=random(-height/4, height/4);
}
