//Primitivas formales:arc() y PGraphics
//Creamos un objeto PGraphics del arco para utilizarlo como imagen y
//rotar su posición en el canvas. El resultado es el mismo gráficamente

float d;
PGraphics pg;
int change;

void setup() {
  size(500, 500);
  d = width*0.8;
  pg = createGraphics(int(d), int(d)); //Generamos un PGraphics del arco que utilizaremos
  pg.beginDraw();
  pg.background(0);
  pg.noStroke();
  pg.arc(0, 0, d, d, 0, HALF_PI);
  pg.endDraw();
}

void draw() {   
  if (frameCount%10 == 0) {
    change += 1;
    if (change >= 4) {
      change = 0;
    }
  }
  background(0);
  translate(width/2, height/2);
  rotate(radians(change*90));
  image(pg, 0, 0);
}
