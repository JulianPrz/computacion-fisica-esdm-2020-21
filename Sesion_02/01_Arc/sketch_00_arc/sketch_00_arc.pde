//Primitivas formales: arc();
//Al pinchar con el ratón en el canvas cambiamos ángulo del arco

int x, y;
int changeShape = 0;

void setup() {
  size(200, 200);
  x = width/2;
  y = height/2;
}

void draw() {
  switch(changeShape) {
  case 0:
    background(0);
    arc(x, y, width*0.8, height*0.8, 0, HALF_PI); //radians(90)
    break;
  case 1:
    background(0);
    arc(x, y, width*0.8, height*0.8, HALF_PI, PI);
    break;
  case 2:
    background(0);
    arc(x, y, width*0.8, height*0.8, PI, PI+HALF_PI);
    break;
  case 3:
    background(0);
    arc(x, y, width*0.8, height*0.8, PI+HALF_PI, TWO_PI);
    break;
  }
}

void mousePressed() {
  changeShape += 1;
  if (changeShape >= 4) {
    changeShape = 0;
  }
}
