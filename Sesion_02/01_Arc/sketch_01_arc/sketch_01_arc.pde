//Primitivas formales: arc()
//Hacemos lo mismo pero utilizamos frameCount para que cambie cada 10frames
//y translate() para situar el punto 0,0 en el centro del canvas

int change = 0;
float d;

void setup() {
  size(200, 200);
  d = width*0.8;
}

void draw() {
  background(0);
  translate(width/2, height/2);
  if (frameCount%10 == 0) {
    change += 1;
    if (change >= 4) {
      change = 0;
    }
  }
  arc(0, 0, d, d, 0+radians(change*90), HALF_PI+radians(change*90));
}
