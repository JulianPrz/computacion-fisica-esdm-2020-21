PImage img;
int w, h, s;
int gridSize = 5;

void settings() {
  img = loadImage("test.jpg");
  s = 2; //Factor de escala
  w = img.width/s;
  h = img.height/s;
  size(w, h);
}

void setup() {
  img.loadPixels();
}

void draw() {
  gridSize = int(map(mouseX,0,w,1,10));
  for (int i = 0; i < img.pixels.length; i++) {
    int x = i%(img.width/gridSize)*gridSize;
    int y = i/(img.width/gridSize)*gridSize;

    if (y < h) {
      noStroke();
      fill(img.pixels[x*s+w*s*y*s]); 
      rect(x, y, gridSize, gridSize);
    }
  }
}
