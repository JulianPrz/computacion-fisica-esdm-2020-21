PImage img;
int gridSize = 5; // variable del tamaño del celda

void settings() {
  img= loadImage("test.jpg"); // Cargamos la imagen
  size(img.width, img.height); // Tamaño del canvas adaptado al tamaño de la imagen
}

void setup() {
  //image(img, 0, 0); // esto dibuja la imagen de fondo
  //background(12, 34, 52); // esto le mete un color de fondo
  img.loadPixels(); //carga la info de los pixeles al array pixels[]

  println(img.width);
  println(img.height);
  println(img.pixels.length);
  noStroke();
}

void draw() {
  background(12, 34, 52); // esto le mete un color de fondo

  gridSize = int(map (mouseX, 0, width, 1, 20)); //mapeamos valores mouseX con el ancho de la celda de retícula
  //gap = int(map (mouseY, 0, height, 1, 20)); //mapeamos valores mouseY con el tamaño del hueco entre celdas


  for (int i=0; i<img.pixels.length; i++) {
    int x = i % (img.width/gridSize) * gridSize; //Módulo:0,1,2,3.....71,72,0,1,2,3... //71 columnas de 7 px de ancho(5+2)
    int y = i / (img.width/gridSize) * gridSize; //División:0,1,2,3...664,665,666 //95 filas de 7 px de altura(5+2)

    if (y < img.height) {
      fill(img.pixels[x + y*img.width]); //
      rect(x, y, gridSize, gridSize);
    }
  }
}
