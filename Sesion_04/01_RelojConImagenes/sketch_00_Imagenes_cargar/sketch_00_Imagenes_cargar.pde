PImage im[] = new PImage[2];

void setup() {
  size(720, 360);
  im[0] = loadImage("foto1.png");
  im[1] = loadImage("foto2.png");
}

void draw() {
  image(im[0], 0, 0);
  image(im[1], 360, 0);
}
