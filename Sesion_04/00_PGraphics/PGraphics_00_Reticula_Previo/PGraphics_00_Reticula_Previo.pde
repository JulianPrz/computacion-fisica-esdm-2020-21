/*Creamos una retícula de arcos con un for anidado
en vez de crear un arco con su posición y ángulos lo que hacemos es
crear un objeto PGraphics donde capturamos la imagen del arco
y después desplazamos ese objeto por la retícula y girandolo aleatoriamiente
en ángulos de 0,90,180,270 grados
*/

PGraphics pg;

void setup() {
  size(400, 400);

  pg = createGraphics(40, 40); //Generamos un PGraphics del arco que utilizaremos
  pg.beginDraw();
  pg.noFill();
  pg.stroke(0, 0, 255);
  pg.strokeWeight(1);
  pg.arc(0, 0, 80, 80, 0, HALF_PI);
  pg.endDraw();
  generate();
}

void draw() {
}

void generate() {
 
  for (int i=0; i < width-20; i=i+40) {
    for (int j=0; j < height-20; j=j+40) {
      image(pg,i, j);
    }
  }
}
