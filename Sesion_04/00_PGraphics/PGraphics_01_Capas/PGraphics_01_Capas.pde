/* Creamos dos objetos PGraphics y cambiamos entre ellos 
para que se mantenga la superposición del pg2 sobre el pg1
 */
 
PGraphics pg1, pg2;
int currentLayer = 1;

void setup() {
  size(600, 600);
  pg1=createGraphics(width, height);
  pg2=createGraphics(width, height);
}

void draw() {
  background(0);

  if (currentLayer == 1) {
    drawOnLayer(pg1, color(200, 200, 0));
  } else if (currentLayer==2) {
    drawOnLayer(pg2, color(0, 200, 200));
  }

  image(pg1, 0, 0);
  image(pg2, 0, 0);
}
void keyPressed() {
  if (key=='1') {
    currentLayer = 1;
  } else if (key=='2') {
    currentLayer = 2;
  }
}

void drawOnLayer(PGraphics pg, color col) {
  pg.beginDraw();
  pg.fill(col);
  pg.noStroke();
  if (mousePressed) pg.ellipse(mouseX, mouseY, 40, 40);
  pg.endDraw();
}
