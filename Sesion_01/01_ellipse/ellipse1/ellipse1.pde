//Sesión 1. Funciones y Variables
//Ellipse 1

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid


void setup() {
  size(300,300);
}

void draw() {
  //background(255);
  //stroke(int(random(255)),int(random(255)),int(random(255)));
  ellipse(mouseX, mouseY, 10,10);
}
