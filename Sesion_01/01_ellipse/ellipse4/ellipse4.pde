//Sesión 1. Funciones y Variables
//Ellipse 4
//Propuesta de Sara - Arcoiris

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid

int d;
int h;

void setup() {
  size(300,300);
  colorMode(HSB,100); //Pasamos a colorMode HSB HueSaturationBrightness para en Hue recorrer el espectro
}

void draw() {
  noStroke();
  
  if(h>=100){
    h = 0; //Reseteo de Hue
  }
  
  fill(h,100,100);
  d = int(random(50));
  ellipse(mouseX, mouseY, d,d);
  h++; //Incremento de 1 en cada loop
}
