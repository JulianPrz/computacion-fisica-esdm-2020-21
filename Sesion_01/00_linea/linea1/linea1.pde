//Sesión 1. Funciones y Variables
//01

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid

// Intentamos sacar la línea opuesta y la pintamos de azul con stroke();
 line(0,0,width,height);

// Después dibujamos la línea horizontal y vertical formando una cruz como el símbolo "+"
// y lo pintamos de color rojo
