import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class linea1 extends PApplet {
  public void setup() {
//Sesión 1. Funciones y Variables
//01

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid

// Intentamos sacar la línea opuesta y la pintamos de azul con stroke();
 line(0,0,width,height);

// Después dibujamos la línea horizontal y vertical formando una cruz como el símbolo "+"
// y lo pintamos de color rojo
    noLoop();
  }

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "linea1" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
