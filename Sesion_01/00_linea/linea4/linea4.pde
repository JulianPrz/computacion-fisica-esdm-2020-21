//Sesión 1. Funciones y Variables
//04

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid

void setup() {
  background(0);
  //frameRate(5);
}

void draw() {
  background(0);
  stroke(255);
  line(round(random(width)), round(random(height)), round(random(width)), round(random(width)));
}
