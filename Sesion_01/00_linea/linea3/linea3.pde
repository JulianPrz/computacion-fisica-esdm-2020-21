//Sesión 1. Funciones y Variables
//03

//Máster en Diseño Interactivo
//Asignatura: Computación Física
//Imparte: Julián Pérez - info@julian-perez.com  
//Escuela Superior de Diseño de Madrid

void setup() {
  stroke(255,0,0);
}

void draw() {
  background(255);
  line(0, 0, mouseX, mouseY);
}
